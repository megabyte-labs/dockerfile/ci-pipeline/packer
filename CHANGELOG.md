# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.7.3](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer/compare/v1.7.2...v1.7.3) (2021-06-16)

### [1.7.2](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer/compare/v21.6.0...v1.7.2) (2021-06-14)

## [21.6.0](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer/compare/v0.0.25...v21.6.0) (2021-06-14)

### [0.0.25](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer/compare/v0.0.24...v0.0.25) (2021-06-13)

### [0.0.24](https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer/compare/v0.0.23...v0.0.24) (2021-06-13)

### 0.0.23 (2021-05-13)
