FROM alpine:3 as build

ENV PACKER_RELEASE_URL https://releases.hashicorp.com/packer/

RUN apk --no-cache add \
      bash~=5
SHELL ["/bin/bash", "-eo", "pipefail", "-c"]
RUN apk --no-cache add \
      curl~=7 \
      unzip~=6 \
      upx~=3 \
  && URL_REGEX="((([[:digit:]]+\.)+)[[:digit:]]+)" \
  && ZIP_REGEX="[[:print:]]+linux_amd64.zip" \
  && DEST_DIR="/tmp" \
  && FILE_NAME="packer.zip" \
  && RELEASES=$(curl "${PACKER_RELEASE_URL}") \
  && [[ $RELEASES =~ $URL_REGEX ]] \
  && LATEST_VERSION="${BASH_REMATCH[0]}" \
  && CHECKSUMS=$(curl "${PACKER_RELEASE_URL%/}/${LATEST_VERSION}/packer_${LATEST_VERSION}_SHA256SUMS") \
  && [[ $CHECKSUMS =~ $ZIP_REGEX ]] \
  && CHECKSUM_DEB="${BASH_REMATCH[0]}" \
  && CHECKSUM="${CHECKSUM_DEB%  packer_${LATEST_VERSION}_linux_amd64.zip}" \
  && curl "${PACKER_RELEASE_URL%/}/${LATEST_VERSION}/packer_${LATEST_VERSION}_linux_amd64.zip" -o "${DEST_DIR}/${FILE_NAME}" \
  && echo "${CHECKSUM}  ${DEST_DIR}/${FILE_NAME}" | sha256sum -c \
  && unzip "${DEST_DIR}/${FILE_NAME}" -d /usr/local/bin \
  && upx /usr/local/bin/packer

FROM alpine:3

ENV container docker

COPY --from=build /usr/local/bin/packer /usr/local/bin/packer

RUN apk --no-cache add \
      bash~=5 \
      curl~=7 \
      git~=2

WORKDIR /work
ENTRYPOINT ["packer"]
CMD ["--version"]

ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Node.js files/configurations that support the creation of Dockerfiles"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/dockerfile/ci-pipeline/packer.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="ci-pipeline"
